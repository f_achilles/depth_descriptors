function [depthSequence, filename]= loadDepthSequence(seqPath,type)
% loadDepthSequence loads different types of stored depth sequences.
% call: depthSequence = loadDepthSequence(file,type)
%
% PNG type:
%   
% MAT type:
%
% ONI type:
%
% KinectV2 type:
switch lower(type)
    case 'png'
        imageFiles = dir([seqPath filesep '*.png']);
        numFiles = numel(imageFiles);
        if numFiles~=0
            pngName = imageFiles(1).name;
            tmp = importdata([seqPath filesep pngName]);
            [h,w] = size(tmp);
            depthSequence = zeros(h,w,numFiles,'uint16');
            for i = 1:numFiles
                pngName = imageFiles(i).name;
                depthSequence(:,:,i) = importdata([seqPath filesep pngName]);
            end
        else
            error('no .png files found in current folder') 
        end
        filesepIndices=strfind(seqPath,filesep);
        if filesepIndices(end)==numel(seqPath)
            seqPath=seqPath(1:(end-1));
            filesepIndices=filesepIndices(1:(end-1));
        end
        filename = seqPath((filesepIndices(end)+1):end);
    case 'mat'
        matFiles = dir([seqPath filesep '*.mat']);
        if numel(matFiles)~=0
            disp('Choose the recording to be replayed')
            for iFiles = 1:numel(matFiles)
                disp([num2str(iFiles) ': ' matFiles(iFiles).name])
            end
            indFile = input('Input valid number and press enter: ');
            load([seqPath filesep matFiles(indFile).name]) 
            filename = matFiles(indFile).name;
            filename = filename(1:(end-4));
        elseif strcmp('.mat',seqPath((end-3):end))
            load(seqPath)
            filename=seqPath((find(strcmp(filesep,num2cell(seqPath)),1,'last')+1):end);
        else
            error('no .mat files found in current folder') 
        end
        depthSequence = depthContainer;
    otherwise
        error('Define a valid data type for depth seqences. ''png'' and ''mat'' are currently available.')
end

end