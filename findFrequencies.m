function Frequencies = findFrequencies(videoPartsCell, options)
options.deltaF = options.FPS/options.tsize;
deltaF = options.deltaF;
tsize = options.tsize;
numParts = numel(videoPartsCell);
Frequencies = zeros(1,numParts);

% calculate FFTs in parallel
parfor i = 1:numParts
    % remove depth-offset
    meanFreePart = videoPartsCell{i}-repmat(mean(videoPartsCell{i},3),1,1,tsize)
%     % normalize depth amplitude
%     dAmplitude = max(max(max(abs(meanFreePart))));
%     if dAmplitude > 1000*eps
%         meanFreePart = meanFreePart/dAmplitude;
%     end
    fourierTmp = fft(meanFreePart,[],3);
    sumOverHeightAndWidth = sum(sum(abs(fourierTmp),1),2);
    [~,freqIndex] = max(sumOverHeightAndWidth(1:floor(end/2)));
    Frequencies(i) = deltaF * (freqIndex-1);
end



end