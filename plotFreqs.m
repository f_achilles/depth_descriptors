function plotHandle = plotFreqs(freqs, depthSequence, options)

[h,w,t] = size(depthSequence);

patchHeight = options.height;
patchWidth = options.width;
patchDuration = options.tsize;
Tind = 0;

% [hs, ws, ts] = sphere(20);
% hs = hs*patchHeight/2;
% ws = ws*patchWidth/2;
% ts = ts*patchDuration/2;

numHParts = floor(h/patchHeight);
numWParts = floor(w/patchWidth);
numTParts = floor(t/patchDuration);

figure;
depthFrame = depthSequence(:,:,1);
hI=imshow(depthFrame,[0 5000]);
% hI=image(depthFrame);
hold on
hPatch=surf(0:patchWidth:(numWParts*patchWidth),0:patchHeight:(numHParts*patchHeight),ones(numHParts+1,numWParts+1),ones(numHParts,numWParts),'facealpha',0.5);
hold off
% colormap magic
colormap([gray(5001);jet(256)])
while(true)
for i = 1:(numTParts*patchDuration)
    
    depthFrame = depthSequence(:,:,i);
    depthFrame(depthFrame>5000)=5000;
    set(hI,'cdata',depthFrame);
    xlabel('X AXIS for SURF')
    ylabel('Y AXIS for SURF')
    oldTind = Tind;
    Tind = ceil(i/patchDuration);
    
    if oldTind ~= Tind
%         Hinds=repmat(1:numHParts,numWParts,1);
%         Hinds=Hinds(:)';
        Winds=repmat(1:numWParts,numHParts,1);
        Winds=Winds(:)';
        fVals = freqs(sub2ind([numHParts, numWParts, numTParts], repmat(1:numHParts,1,numWParts), Winds, repmat(Tind,1,numHParts*numWParts)));
        patchSurface = reshape(fVals,numHParts,numWParts);
        cSurface = 256*patchSurface/max(freqs(:)) + 5001;
        set(hPatch,'cdata',cSurface);
    end
    caxis([0 5257]);
%     for Hind = 1:numHParts
%         for Wind = 1:numWParts
%             fVal = freqs(sub2ind([numHParts, numWParts, numTParts], Hind, Wind, Tind));
% %             if oldTind ~= Tind
%             hold on
%             surf(ws+((Wind-0.5)*patchWidth),hs+((Hind-0.5)*patchHeight),ts,'edgecolor','none','facecolor',[fVal, 0, max(freqs)-fVal]/max(freqs),'facealpha',fVal/max(freqs));%exp(-(fVal-9)^2))
%             hold off
% %             end
%         end
%     end
    axis image
    % 20Hz framerate
    pause(0.05)
    
end

pause
plotHandle = gcf;
end

end