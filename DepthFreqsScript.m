close all;
% clear all;
%% perform frequency extraction from depth video

depthSequence = loadDepthSequence('C:\Users\Felix\Projects\Kinect Garching\matlabRecordings','mat');
% depthSequence = loadDepthSequence('C:\Users\Felix\Downloads\HON4D\HON4D\Data\MSRAction\VideosCrop\action01\d_a01_s01_e01_sdepth','png');

options.height = 100;
options.width = 100;
options.tsize = 90;
options.FPS = 30;
videoPartsCell = makeSequenceParts(double(depthSequence), options);

freqs = findFrequencies(videoPartsCell, options);

plotFreqs(freqs, depthSequence, options);