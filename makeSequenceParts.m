function videoPartsCell = makeSequenceParts(DepthSequence, options)
height = options.height;
width = options.width;
tsize = options.tsize;
sizeOfDepthSequence = size(DepthSequence);
% numXParts = sizeOfDepthSequence(1)-xsize+1;
% numYParts = sizeOfDepthSequence(2)-ysize+1;
% numTParts = sizeOfDepthSequence(3)-tsize+1;
% numParts = numXParts*numYParts*numTParts;
% videoPartsCell = cell(1,numParts);
% parfor partInd = 1:numParts
%     []ind2sub(partInd,)
%     videoPartsCell{i} = 
% 
% end

numHParts = floor(sizeOfDepthSequence(1)/height);
numWParts = floor(sizeOfDepthSequence(2)/width);
numTParts = floor(sizeOfDepthSequence(3)/tsize);
if numTParts == 0
    error(['recording shorter than ' num2str(tsize/options.FPS) ' seconds!']) 
end
numParts = numHParts*numWParts*numTParts;
videoPartsCell = cell(1,numParts);
parfor partInd = 1:numParts
    [Hind, Wind, Tind] = ind2sub([numHParts, numWParts, numTParts],partInd);
    videoPartsCell{partInd} = DepthSequence(((Hind-1)*height+1):(Hind*height),((Wind-1)*width+1):(Wind*width),((Tind-1)*tsize+1):(Tind*tsize));
end

end